import $ from "jquery";
import $$ from 'dom7';
import Framework7 from 'framework7/framework7.esm.bundle.js';

import Login from '../pages/login.f7.html';
import Signup from '../pages/signup.f7.html';
import Signupform from '../pages/signupform.f7.html';
import Signupotp from '../pages/signupotp.f7.html';
import Account from '../pages/account.f7.html';
import AccountEdit from '../pages/account-edit.f7.html';
import HomePage from '../pages/home.f7.html';
import BidPage from '../pages/bid-page.f7.html';
import PageTerkini from '../pages/page-terkini.f7.html';
import PagePopular from '../pages/page-popular.f7.html';
import PageBerakhir from '../pages/page-berakhir.f7.html';
import HistoryBid from '../pages/history-bidauction.f7.html';
import CreateAuction from '../pages/create-auction.f7.html';
import CreateAuctionImg from '../pages/create-auction-img.f7.html';
import CreateAuctionDesc from '../pages/create-auction-desc.f7.html';
import Notification from '../pages/notification.f7.html';
import History from '../pages/history.f7.html';
import Wallet from '../pages/wallet.f7.html';
import Transaction from '../pages/transaction.f7.html';
import DetailTransaction from '../pages/transaction-detail.f7.html';
import FollowedDetailTransaction from '../pages/transaction-followed-detail.f7.html';
import Dispute from '../pages/page-dispute.f7.html';

var routes = [
  {
    path: '/login/',
    component: Login
  },
  {
    path: '/home/',
    component: HomePage
  },
  {
    path: '/bid_page/:bid_id',
    component: BidPage
  },
  {
    path: '/create/',
    component: CreateAuction
  },
  {
    path: '/create-item-cond/',
    component: CreateAuctionDesc
  },
  {
    path: '/create-img/',
    component: CreateAuctionImg
  },
  {
    path: '/notification/',
    component: Notification
  },
  {
    path: '/wallet/',
    component: Wallet
  },
  {
    path: '/signup/',
    component: Signup
  },
  {
    path: '/signupform/',
    component: Signupform
  },
  {
    path: '/page-terkini/',
    component: PageTerkini
  },
  {
    path: '/page-popular/',
    component: PagePopular
  },
  {
    path: '/page-berakhir/',
    component: PageBerakhir,
  },
  {
    path: '/historyBid/:auction_id',
    component: HistoryBid
  },
  {
    path: '/account/',
    component: Account
  },
  {
    path: '/account-edit/',
    component: AccountEdit
  },
  {
    path: '/history/',
    component: History
  },
  {
    path: '/otp/',
    component: Signupotp
  },
  {
    path: '/dispute/',
    component: Dispute
  },
  {
    path: '/transaction/',
    component: Transaction
  },
  {
    path: '/detail-transaction/:trx_id',
    component: DetailTransaction
  }
  // {
  //   path: '/detail-followed-transction/:ftrx_id',
  //   component: FollowedDetailTransaction
  // }
];

export default routes;