import $$ from 'dom7';
import $ from "jquery";

import Framework7 from 'framework7/framework7.esm.bundle.js';
import cordovaApp from './cordova-app.js';
import routes from './routes.js';

import 'framework7/css/framework7.bundle.css';
import '../css/icons.css';
import '../css/custom.css';
import '../css/app.styl';

var userId = "userId";

var app = new Framework7({
  root: '#app', 
  id: 'io.framework7.myapp', 
  name: 'My App', 
  theme: 'auto', 
  routes: routes,
  panel: {
    leftBreakpoint: 960,
  },
  input: {
    scrollIntoViewOnFocus: Framework7.device.cordova && !Framework7.device.electron,
    scrollIntoViewCentered: Framework7.device.cordova && !Framework7.device.electron,
  },
  statusbar: {
    overlay: Framework7.device.cordova && Framework7.device.ios || 'auto',
    iosOverlaysWebView: true,
    androidOverlaysWebView: false,
  },
  touch: {
    tapHold: true
  },
  on: {
    init: function () {
      var f7 = this;
      if (f7.device.cordova) {
        cordovaApp.init(f7);
      }

      f7.views.create('.view-main', {
          url: '/home/'
      });
    },
    pageBeforeIn: function() {
      
    },
    pageAfterIn: function() {
      
    }
  },
  data: function() {
    var host = 'http://bidu.dcsys.id:80/';
    // var host = 'http://127.0.0.1:3000/';
    // var host = 'http://156.67.216.178:3007/'; //staging
    return {
      host
    }
  },
});
